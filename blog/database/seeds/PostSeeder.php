<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Post::class,10)->create()->each(function($u){ // build a post class, make it 10 posts and create them by saving them in the database
            $u->make(); //inside each , save an issues assoicated with the post
        });
    }
}


