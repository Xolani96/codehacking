<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct(){

        $this->middleware('CheckAge');
    }
    public function listuser(){
        return "list users - admin/users controller";
    }
}
